let currentItemsSet;
let flagCurrentSortingMode = 0; // 1 for "By Date" state, 2 for "By Episodes" state
let flagSortByDateASC = false; // Descending by default
let lastAddedCharacterId = 10;
//let currentTenCharacters;
let deletedCharacters = [];
let flagLoadedMoreThenTen = false;
const baseUrl = 'https://rickandmortyapi.com/api/character/';
const url = 'https://rickandmortyapi.com/api/character/1,2,3,4,5,6,7,8,9,10'; 

window.onload = () => {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const request = new Request(url, {
        method: 'GET', headers
    });

    fetch(request)
        .then(response => {
            response.json().then(cards => {
                //console.log(cards);
                currentItemsSet = cards;
                //redrawCharacters(currentTenCharacters);
                redrawCharactersTable(currentItemsSet);


                //const tempbutton = document.getElementById('loadMoreBtn');// переробити без змінної
                document.querySelector('#loadMoreBtn').addEventListener('click', () => {
                    loadMoreItems();
                });

                document.querySelector('#sort-episodes-button').addEventListener('click', () =>{
                    sortByEpisodes();
                    redrawCharactersTable(currentItemsSet);
                });

                document.querySelector('#sort-date-button').addEventListener('click', () =>{
                    sortByDate();
                    redrawCharactersTable(currentItemsSet);
                });

                document.querySelector('#clearBtn').addEventListener('click', () =>{
                    revertToTen();
                    redrawCharactersTable(currentItemsSet);                    
                });

                //Adding Event for elastic search
                document.querySelector('#elastic_search').addEventListener('input', (evt) =>{
                    handleElasticSearch(evt.target.value);
                });
            });
        })
        .catch(err => {
            console.log(err);
        });
};
//Elastic search functionality
function handleElasticSearch(enteredValue){
                                        
    let searchQuery = enteredValue.trim().toLowerCase();
    let elasticItems = document.querySelector('.table_body');

    if (searchQuery != ''){
      for(let node of elasticItems.childNodes){
           let text = node.firstChild.innerText.toLowerCase();
           if(text.indexOf(searchQuery) == -1)
                node.style.display = 'none';  
           else
                node.style.display = null;
       }   
    }
    else{
        for(let node of elasticItems.childNodes){
            node.style.display = null;
        }
    }
}

// Removing Item from current set of items and hiding displaying element
function removeCharacter(characterID){
    const itemToHide = document.getElementById('tbItem'+characterID);
    itemToHide.classList.add('hide');   //hiding element without redrawing the whole table
    deletedCharacters.push(characterID);
    let indexToDelete = currentItemsSet.findIndex(item => item.id == characterID);
    currentItemsSet.splice(indexToDelete, 1);

    if(currentItemsSet.length <= 10)
        document.querySelector('#clearBtn').classList.add('hide');
}

function sortByDate() {
    if(flagSortByDateASC){
        currentItemsSet.sort((a,b)=> a.created > b.created ? 1 : -1);
        flagSortByDateASC = false;
    }
        
    else{
        currentItemsSet.sort((a,b)=> a.created < b.created ? 1 : -1);
        flagSortByDateASC = true;
    }
    flagCurrentSortingMode = 1;    
}

function sortByEpisodes() {
    for(let i = 0; i < currentItemsSet.length; i++){
        console.log(currentItemsSet[i].name + ". Episodes: " + currentItemsSet[i].episode.length);
    }
    let res2 = currentItemsSet.sort((a,b)=> {
        if(a.episode.length === b.episode.length){
            return a.created < b.created ? 1 : -1
        }else {
            return a.episode.length < b.episode.length ? 1 : -1
        }
    });
    flagCurrentSortingMode = 2; 
}

function loadMoreItems(){

    let url = 'https://rickandmortyapi.com/api/character/';
    let nextCharacter = lastAddedCharacterId + 1;
    for(let i = 0; i < 10; i++){
        url += nextCharacter + ',';
        nextCharacter++;
    }
    url = url.slice(0, url.length-1); //removing the last comma
    lastAddedCharacterId += 10;

    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const request = new Request(url, {
        method: 'GET', headers
    });

    fetch(request)
        .then(response => {
            response.json().then(newCards => {
                
                //Adding new items to the current set of characters
                for(let i = 0; i < newCards.length; i++){
                    currentItemsSet.push(newCards[i]);
                }

                if(flagCurrentSortingMode == 1) // Current mode is in Sort By Date
                    sortByDate();
                else if (flagCurrentSortingMode == 2) // Current mode is in Sort by Episode
                    sortByEpisodes();

                redrawCharactersTable(currentItemsSet);
                if(currentItemsSet.length > 10)
                    document.querySelector('#clearBtn').classList.remove('hide');
            });
        })
        .catch(err => {
            console.log(err);
        });
}

function revertToTen(){
    currentItemsSet.splice(10, currentItemsSet.length-1);    
}


function redrawCharactersTable(charactersList) {
    console.log("In redrawing");
    let tableContainer = document.body.getElementsByClassName('table_body')[0];
    tableContainer.innerHTML = "";
    for(let i = 0; i < charactersList.length; i++) {
        let Item = document.createElement('tr');
        Item.setAttribute("id", 'tbItem'+charactersList[i].id);
        Item.innerHTML += '<td><p><strong>'+charactersList[i].name + '</strong></p></td>';
        Item.innerHTML += '<td><p class="align-center"><img class="avatar" src="'+charactersList[i].image+'"></p></td>';
        Item.innerHTML += '<td><p class="align-center"><strong>'+charactersList[i].species+'</strong></p></td>';
        Item.innerHTML += '<td><p class="align-center">'+charactersList[i].location.name+'</p></td>';
        let date = new Date(charactersList[i].created);
        Item.innerHTML += '<td><p class="align-center">'+date.toLocaleString()+'</p></td>';
        Item.innerHTML += '<td><p class="align-center">'+charactersList[i].episode.length+'</p></td>';
        Item.innerHTML += '<td><p class="align-center"><button class="button-primary-outlined" onclick="removeCharacter('+charactersList[i].id+')">Remove</button></td>';
        tableContainer.append(Item);
    }
}